module.exports = {
    pluginOptions: {
        quasar: {
            theme: 'mat',
            rtlSupport: true,
            importAll: true
        }
    },
    transpileDependencies: [
        /[\\\/]node_modules[\\\/]quasar-framework[\\\/]/
    ],
    outputDir: 'cordova/www',
    publicPath:''
}
