
import Vue from 'vue'
import Vuex from 'vuex'

import layoutDemo from './layoutDemo/index'

Vue.use(Vuex)

export default function () {
    const Store = new Vuex.Store({
        modules: {
            layoutDemo
        }
    });

    return Store
}
