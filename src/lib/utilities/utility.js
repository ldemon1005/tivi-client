/* eslint-disable */
import Vue from 'vue'
import md5 from 'js-md5'
import Axios from 'axios'
import db from './database'
import moment from 'moment'
import VueRouter from 'vue-router'
import {isNullOrUndefined, isString} from 'util'

const code_http = {
    processing: 102,
    ok: 200,
    completed: 201,
    done: 202,
    created: 204,
    missing_parameters: 400,
    unauthorized: 401,
    forbidden: 403,
    underfined: 404,
    not_allowed: 405,
    request_expired: 408,
    conflict: 409,
    unprocessable: 422,
    resent: 429,
    fail: 500,
    exception: 501,
    unsavedb: 502,
    unavailable: 503
}
function prl(c, n) {
    const l = n > 0 ? n : 150
    let p = c || '-'
    let r = ''
    for (let i = 0; i < l; i++) {
        r += p
    }
    return r
}

require('dotenv').config()

Vue.prototype.moment = moment

const config = {}

function log (...obj) {
    console.time('Duration')
    if (obj.length === 1) {
        const cnt = isString(obj[0]) ? obj[0] : JSON.stringify(obj[0])
        console.log("\n%c"+prl(), "color:DodgerBlue")
        console.info("%c" + cnt, "color:DodgerBlue")
        console.timeEnd('Duration')
        console.log("%c"+prl()+"\n", "color:DodgerBlue")
    } else if (obj.length === 2) {
        const msg = obj[0].toString().toUpperCase()
        let cnt = obj.slice(1)
        // console.log(`\n${prl()}\n${msg}: ${cnt}\n${prl()}\n\n`)
        cnt = isString(cnt) ? cnt : JSON.stringify(cnt)
        console.log("\n%c"+prl(), "color:DodgerBlue")
        console.groupCollapsed(msg)
        console.info("%c" + cnt, "color:DodgerBlue")
        console.timeEnd('Duration')
        console.groupEnd(msg)
        console.log("%c"+prl()+"\n", "color:DodgerBlue")
    } else {
        const msg = obj[0].toString().toUpperCase()
        const arr = obj.slice(1)
        if (arr) {
            let i = 0
            console.log("\n%c"+prl('='), "color:Green")
            console.group(msg)
            console.info("%c"+prl('˜'),"color:Green")
            arr.map(o => {
                i += 1
                console.groupCollapsed(`Item ${i}`)
                if (_.isObject(o)) {
                    console.log(`${JSON.stringify(o)}`)
                } else if (isString(o)) {
                    console.log(`${o}`)
                } else {
                    console.log(o)
                }
                if (i < arr.length - 1) {
                    console.log(`${prl('-')}`)
                }
                console.groupEnd(`Item ${i}`)
                return o
            })
            console.log("%c"+prl('_'),"color:Green")
            console.warn(`Total: ${arr.length}`)
            console.timeEnd('Duration')
            console.groupEnd(msg)
            console.log("%c"+prl()+"\n", "color:Green")
        }
    }
}

const lg = (...obj) => {
    let val = null
    let lbl = 'Variable'
    let cfg = 0
    if (obj.length === 1) {
        val = obj[0]
    } else if (obj.length === 2) {
        lbl = obj[0].toString().toUpperCase()
        val = obj[1]
    } else {
        lbl = obj[0].toString().toUpperCase()
        cfg = obj[(obj.length - 1)]
        val = obj.slice(1)
        if (!isNaN(cfg) && parseInt(cfg, 10) > 0 && parseInt(cfg, 10) < 5) {
            cfg = parseInt(cfg, 10)
            val = val.slice(-1)
        }
    }
    if (cfg === 4) {
        console.clear()
        console.trace()
    } else if (cfg === 3) {
        console.log("\n%c"+prl('=')+"\n"+lbl+"\n"+prl('.'), "color:Red")
        console.error(JSON.parse(JSON.stringify(val)))
        console.log("%c"+prl('=')+"\n", "color:Red")
    } else if (cfg === 2) {
        console.log("\n%c"+prl('=')+"\n"+lbl+"\n"+prl('-'), "color:DoubleBlue")
        console.info(JSON.parse(JSON.stringify(val)))
        console.log("%c"+prl('=')+"\n", "color:DoubleBlue")
    } else if (cfg === 1) {
        console.log("\n%c"+prl('=')+"\n"+lbl+"\n"+prl('-'), "color:Orange")
        console.warn(JSON.parse(JSON.stringify(val)))
        console.log("%c"+prl('=')+"\n", "color:Orange")
    } else {
        console.log("\n%c"+prl('=')+"\n"+lbl+"\n"+prl('-'), "color:Green")
        console.log(JSON.parse(JSON.stringify(val)))
        console.log("%c"+prl('=')+"\n", "color:Green")
    }
}


const c = env => {
    if (Object.entries(env).length > 0 && typeof env === 'object') {
        Object.entries(env).forEach(([key, val]) => {
            let k = key
            let v = val
            let p = ''
            let pref = ''
            let alias = key.substr(8)
            if (alias.substr(0, 5) === 'MEDIA') {
                pref = alias.substr(0, 5).toLowerCase()
                k = alias.substr(6).toLowerCase()
                const o = alias.substr(6).split('_')
                k = o[0].toLowerCase()
                p = o[1].toLowerCase()
                v = {}
                if (alias.substr(-4) === 'TYPE') {
                    v[p] = val.split(',')
                } else if (alias.substr(-4) === 'SIZE') {
                    v[p] = parseInt(val)
                } else {
                    v[p] = isNaN(val) ? val : parseInt(val)
                }
            } else {
                pref = 'app'
                k = key.substr(8)
                k = k.substr(0, 3) === 'APP' ? k.substr(4).toLowerCase() : k.toLowerCase()
                v = isNaN(val) ? val : parseInt(val)
            }
            if (pref && k) {
                config[pref] = config[pref] && typeof config[pref] === 'object' ? config[pref] : {}
                if (typeof v === 'object') {
                    config[pref][k] = config[pref][k] && typeof config[pref][k] === 'object' ? config[pref][k] : {}
                    config[pref][k][Object.keys(v)[0]] = v[Object.keys(v)[0]]
                } else {
                    config[pref][k] = typeof v === 'object' ? v : v
                }
            } else if (k && v) {
                config[k.toLowerCase()] = v
            }
        })
    }
}

c(process.env)

const baseUrl = config.app.service_api

const USERS_COOKIE = '__ck'
const TOKEN_KEY_ID = 'token'

let temp = {}
temp = {
    calling: {}
}

const bus = new Vue()

const o = new Vue()

const token = () => {
    let resp = db.c.g(TOKEN_KEY_ID)
    const valid = db.l.g(USERS_COOKIE)
    return resp && resp !== '' && resp === valid ? resp : valid ? valid : resp
}

function live(variable) {
    return (typeof variable !== 'undefined');
}

const a = unauthorize => {
    const check = isNullOrUndefined(unauthorize) || unauthorize === '' || parseInt(unauthorize) === 0
    Axios.defaults.headers.common['Accept'] = 'application/json';
    Axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
    if (check) {
        const tokenKey = token()
        if (tokenKey) Axios.defaults.headers.common['Authorization'] = tokenKey
    } else {
        if (is.has(Axios.defaults.headers.common, 'Authorization')) delete Axios.defaults.headers.common['Authorization']
        if (unauthorize === 2) Axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
    }

    return Axios
}

const g = (link, attributes = null, unauthorize = false, blob = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = ''
        if (!unauthorize) {
            hash = md5(`${link}${JSON.stringify(attributes)}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            link = `${config.app.service_api}/api/${link}`
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            const payload = {
                params: attributes
            }
            if (blob) {
                payload.responseType = 'blob'
            }
            a().get(link, payload).then(response => {
                let result = response.data
                if (!unauthorize && live(temp.calling) && is.has(temp.calling, hash)) {
                    delete temp.calling[hash]
                }
                if (!blob && result.code && result.code !== 200) {
                    alert(`Không thể truy xuất được dữ liệu từ máy chủ vì:\n\n*${result.message}*`)
                    // error(result)
                    reject(result)
                } else {
                    resolve(result)
                }
            }).catch(e => {
                resolve(e);
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
})

const p = (link, params = null, environment = "baseUrl", unauthorize = false, full_response = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = '';
        if (!unauthorize) {
            hash = md5(`${link}${JSON.stringify(params)}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            link = `${config.app.service_api}/api/${link}`;
            a().post(link, params).then(response => {
                const result = response.data;
                if (!full_response) {
                    const result = response.data;
                    if (result.code && result.code !== 200) {
                        alert(`Không thể truy xuất được dữ liệu từ máy chủ vì lỗi:\n\n*${result.message}*`)
                        // error(result)
                        reject(result)
                    } else {
                        resolve(result.data)
                    }
                } else {
                    resolve(result)
                }
            }).catch(e => {
                reject(e)
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
});

const put = (link, params = null, unauthorize = false, full_response = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = ''
        if (!unauthorize) {
            hash = md5(`${link}${JSON.stringify(params)}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            link = `${config.app.service_api}/api/${link}`
            a().put(link, params).then(response => {
                const result = response.data
                if (!full_response) {
                    if (result.code && result.code !== 200) {
                        alert(`Không thể truy xuất được dữ liệu từ máy chủ vì lỗi:\n\n*${result.message}*`)
                        // error(result)
                        reject(result)
                    } else {
                        resolve(result.data)
                    }
                } else {
                    resolve(result)
                }
            }).catch(e => {
                reject(e)
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
})

const t = (link, params, unauthorize = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = ''
        if (!unauthorize) {
            hash = md5(`${link}${JSON.stringify(params)}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            link = `${config.app.service_api}/api/${link}`
            a().put(link, params).then(response => {
                const result = response.data
                if (result.code && result.code !== 200) {
                    alert(`Không thể truy xuất được dữ liệu từ máy chủ vì lỗi:\n\n*${result.message}*`)
                    // error(result)
                    reject(result)
                } else if (result.code == code_http.unauthorized) {
                    router.go('/error_permission')
                } else {
                    resolve(result.data)
                }
            }).catch(e => {
                reject(e)
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
})

const d = (link, unauthorize = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = ''
        if (!unauthorize) {
            hash = md5(`${link}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            link = `${config.app.service_api}/api/${link}`
            a().delete(link).then(response => {
                const result = response.data
                if (live(temp.calling) && is.has(temp.calling, hash)) {
                    delete temp.calling[hash]
                }
                if (result.code && result.code !== 200) {
                    alert(`Không thể truy xuất được dữ liệu từ máy chủ vì lỗi:\n\n*${result.message}*`)
                    // error(result)
                    reject(result)
                } else {
                    resolve(result.data)
                }
            }).catch(e => {
                reject(e)
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
})

const e = (link) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        const tokenKey = token()
        Axios.get(link, {
            headers: {
                Authorization: tokenKey
            },
            responseType: "blob"
        }).then(response => {
            resolve(response.data)
        }).catch(e => {
            reject(e)
        })
    } else {
        reject('Request url is not valid')
    }
})

const currency = (num, cur) =>
    num
        .toFixed(1)
        .replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
        .slice(0, -2) + cur;

const is = {
    in: (obj, key) => obj && Array.isArray(obj) && key ? parseInt(obj.indexOf(key), 10) > -1 : false,
    obj: obj => typeof obj === 'object' && !Array.isArray(obj),
    arr: obj => Array.isArray(obj),
    has: (obj, key) => typeof obj === 'object' && !Array.isArray(obj) ? Object.hasOwnProperty.call(obj, key) : false,
    for: obj => Object.keys(obj)
}

function load(obj) {
    let resp = {}
    if (is.obj(obj) && is.has(obj, 'pages')) {
        if (is.obj(obj.pages)) {
            is.for(obj.pages).map(key => {
                let path = is.has(obj, 'path') ? obj.path + '/' : obj.name + '/'
                path = path == 'pages/' ? '' : path
                const item = obj.pages[key]
                path = is.has(item, 'f') ? path + item.f : path + key
                const page = {
                    path: item.p,
                    name: item.n,
                    component: require('../../views/' + path + '/temp').default
                }
                resp[key] = page
                return key
            })
        }
    }
    return resp
}

function loadRouter(obj) {
    let resp = {};
    if (is.obj(obj) && is.has(obj, "pages")) {
        if (is.obj(obj.pages)) {
            is.for(obj.pages).map(key => {
                let path = is.has(obj, "path") ? obj.path + "/" : obj.name + "/";
                path = path == "pages/" ? "" : path;
                const item = obj.pages[key];
                path = is.has(item, "f") ? path + item.f : path + key;
                let fileName = is.has(item, "f") ? item.fileName : "/temp";
                const page = {
                    path: item.p,
                    name: item.n,
                    component: require("../../views/" + path + "/" + fileName).default
                };
                resp[key] = page;
                return key;
            });
        }
    }
    // log(`Loading ${obj.name}`, resp)
    return resp;
}

const vld = {
    email: (str) => {
        // const pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        const pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
        return pattern.test(str)
    },
    null: (str) => {
        const pattern = /\S+/
        return pattern.test(str)
    },
    num: (str) => {
        // const pattern = /^\d+$/
        const pattern = /^-?\d+\.?\d*$/
        return pattern.test(str)
    },
    cc: (str) => {
        const pattern = /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11}|62[0-9]{14})$/
        return pattern.test(str)
    },
    visa: (str) => {
        const pattern = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/
        return pattern.test(str)
    },
    master: (str) => {
        const pattern = /^(?:5[1-5][0-9]{14})$/
        return pattern.test(str)
    },
    amex: (str) => {
        const pattern = /^(?:3[47][0-9]{13})$/
        return pattern.test(str)
    },
    discover: (str) => {
        const pattern = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/
        return pattern.test(str)
    },
    same: (str1, str2) => {
        return str1 === str2
    }
}


const convertDateToString = date => date.getFullYear() + "-" + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + '' + (date.getMonth() + 1))) + "-" + ((date.getDate() > 8) ? date.getDate() : ('0' + '' + (date.getDate())));

const rd = num => {
    let resp = parseInt(num, 10)
    if (num > 999) {
        const tod = num % 1000
        const nod = num / 1000
        if (tod > 0 && tod < 900) {
            resp = nod * 1000
        } else if (tod > 0 && tod > 900) {
            resp = (nod + 1) * 1000
        }
    }
    return resp
}

const formatMoney = input => input.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

const formatNumber = input => parseInt(input.replace(/,/g, ""));

const genErrorHtml = message => `<span class='text-danger'><i class='fa fa-exclamation-circle'></i> ${message}</span></br>`;

const genSuccessHtml = message => `<span class='text-success'>${message}</span></br>`;

const subString = (x, str_lenght = 15) => {
    var title = x.split(' ');
    var res = '';
    if (title.length > str_lenght) {
        var str_arr = [];
        for (var i = 0; i < str_lenght; i++) {
            str_arr.push(title[i]);
        }
        res = str_arr.join(' ') + ' ...';
    } else {
        res = title.join(' ');
    }
    return res;
}

const getTextStatus = (status) => {
    var str_status = '';
    switch (status) {
        case 0:
            str_status = 'đang xóa';
            break;
        case 1:
            str_status = 'đang hoạt động';
            break;
        case 2:
            str_status = 'đang chờ phê duyệt';
            break;
        case 3:
            str_status = 'đã được cấp phép đăng';
            break;
        case 4:
            str_status = 'đã bị thu hồi';
            break;
    }
    return str_status;
}

const getClassStatus = (status) => {
    var str_status = '';
    switch (status) {
        case 0:
            str_status = 'negative';
            break;
        case 1:
            str_status = 'positive';
            break;
        case 2:
            str_status = 'secondary';
            break;
        case 3:
            str_status = 'primary';
            break;
        case 4:
            str_status = 'warning';
            break;
    }
    return str_status;
}

const getImage = (obj, type) => {
    var link = 'https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100&ssl=1';
    if (obj == null) return link;
    switch (type) {
        case 'thumbLink':
            link = `${config.app.service_api}` + obj.thumbLink;
            break;
        case 'mediumLink':
            link = `${config.app.service_api}` + obj.mediumLink;
            break;
        case 'originLink':
            link = `${config.app.service_api}` + obj.originLink;
            break;
    }
    return link;
}

export default {
    a,
    c,
    e,
    g,
    d,
    p,
    t,
    token,
    config,
    currency,
    load,
    loadRouter,
    getTextStatus,
    getClassStatus,
    getImage,
    baseUrl
};

