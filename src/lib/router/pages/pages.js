import u from "../../utilities/utility";

const routers = u.loadRouter({
    name: "pages",
    pages: {
        /**
         * home
         */
        home: {
            p: "/home",
            n: "home",
            f: "home",
            fileName: "temp"
        },

        detailChannel: {
            p: "/channel/:id",
            n: "channel-detail",
            f: "channel-detail",
            fileName: "temp"
        },

        /**
         * error
         */
        error_404: {
            p: "/404",
            n: "error404",
            f: "error",
            fileName: "Error404"
        },
        error_auth: {
            p: "/error_permission",
            n: "error",
            f: "error",
            fileName: "ErrorAuth"
        }
    }
});
export default {
    routers,
    router: {
        path: "home",
        redirect: "/home",
        name: "Trang chủ",
        component: {
            render(c) {
                return c("router-view");
            }
        },
        children: [
            routers.home,
            routers.detailChannel,

            /**
             * error
             */
            routers.error_404,
            routers.error_auth
        ]
    }
};

