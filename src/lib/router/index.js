import Vue from "vue"
import Router from "vue-router"
import Page from "./pages/pages"
import Master from "./../../views/master/temp"

Vue.use(Router);

const routing = new Router({
    mode: "hash",
    linkActiveClass: "open active",
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: "/",
            redirect: "/home",
            name: "Trang Chủ",
            component: Master,
            children: [Page.router]
        },
        {path: "*", redirect: "/404"}
    ]
});
export default routing;
