import Vue from 'vue'
import App from './App'
import router from '@router'
import store from '@store'
import Utility from '@utility/utility'
import '@style/quasar.styl'
import 'quasar-framework/dist/quasar.ie.polyfills'
import iconSet from 'quasar-framework/icons/fontawesome'
import 'quasar-extras/animate'
import 'quasar-extras/roboto-font'
import 'quasar-extras/material-icons'
import 'quasar-extras/fontawesome'
import 'quasar-extras/ionicons'
import 'quasar-extras/mdi'
import Quasar, * as All from 'quasar'
import i18n from '@lang/i18n'
import LoadScript from 'vue-plugin-load-script';

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'



window.u = Utility
import v from 'vuelidate'

try {
	window.Vue = require('vue')
	window._ = require('lodash')
	window.m = require('moment')
	Vue.use(v)
	Vue.use(Quasar)
	Vue.use(LoadScript)
} catch (err) {
	Utility.lg(err)
}

window.axios = require('axios')

Vue.use(Quasar, {
	config: {},
	iconSet: iconSet,
	components: All,
	directives: All,
	plugins: All
})

Vue.prototype.$eventHub = new Vue()

Vue.config.productionTip = false

Vue.loadScript('https://code.jquery.com/jquery-3.3.1.min.js').then(() => { }).catch(() => { })

new Vue({
	el: '#app',
	router,
	store,
	i18n,
	render: h => h(App)
})
